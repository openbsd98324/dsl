# dsl

damn small linux





Note: ext3 is ok too. You can even use this method on a fat16 formatted partition.

Change to the directory where you mounted the ext2 partition, install GRUB boot loader:
   cd /tmp/pendrive/
   grub-install  --no-floppy  --root-directory=.   /dev/<rootdevice>  
Note: Replace <rootdevice> with the corresponding file which represents your USB storage device WITHOUT the partition number, so that the mbr is updated. For example /dev/sdc

Note 2: This method did not work for me, I used the manual method of running grub and entered "root (hd1,1)" and "setup (hd1)" (your paths will probably vary).

Copy the contents of the cd image to the usb drive.
   mkdir /tmp/dsl-cd
   mount [/path_to/]current.iso /tmp/dsl-cd/ -o loop
   cp -vR /tmp/dsl-cd/* /tmp/pendrive/
While in the same directory - where you mounted the ext2 filesystem - create a menu.lst file for grub in the directory ./boot/grub/menu.lst
  cat > /tmp/pendrive/boot/grub/menu.lst << EOF
  title           Damn Small Linux
  root            (hd0,0)
  kernel          /boot/isolinux/linux24 root=/dev/sda1 ro lang=us toram noeject frugal
  initrd          /boot/isolinux/minirt24.gz
  boot
  EOF
